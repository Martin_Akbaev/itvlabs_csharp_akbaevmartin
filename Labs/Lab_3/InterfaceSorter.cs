﻿using System;
using System.Collections.Generic;

namespace Lab3
{
    public interface ISorter
    {
        List<double> Sort(List<double> unsorted, Delegate key=null, bool reverse=false);
        long compareNumber { get; }
    }
}
