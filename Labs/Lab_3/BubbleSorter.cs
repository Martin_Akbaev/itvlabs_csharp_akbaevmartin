﻿﻿using System;
using System.Collections.Generic;

namespace Lab3
{
    public class BubbleSorter : ISorter
    {
        public long compareNumber { get; private set; }
        
        delegate double MyDeleg(double val);
        MyDeleg defaultKey = (double x) => { return x; };


        public static void Swap<T>(IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }
        public List<double> Sort(List<double> unsorted, Delegate key = null, bool reverse = false)
        {
            key = key ?? defaultKey;

            compareNumber = 0;
            List<double> result = new List<double>();

            for (int i = 0; i < unsorted.Count; ++i)
            {
                for (int j = 0; j < unsorted.Count - i - 1; ++j)
                {
                    compareNumber++;
                    if ((double)key.DynamicInvoke(unsorted[j]) > (double)key.DynamicInvoke(unsorted[j + 1]))
                    {
                        Swap(unsorted, j, j + 1);
                    }
                }
            }
            result = unsorted;
            if (reverse)
            {
                result.Reverse();
            }
            return result;
        }
    }
}
