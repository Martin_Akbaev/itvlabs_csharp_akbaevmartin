﻿using System;
using System.Collections.Generic;

namespace Lab1
{
    public class SumNumbersAfterDot
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Write("Enter Value: ");
                string input = Console.ReadLine();
                if (double.TryParse(input, out double _))
                {
                    List<int> splited = numbersAfterDot(input);
                    Console.WriteLine($"Sum: {Sum(splited)}");
                }
                else
                {
                    Console.WriteLine("Incorrect input!");
                }

                Console.WriteLine("Press any key for continue, press Escape for exit.");

            } while (Console.ReadKey().Key != ConsoleKey.Escape);

        }


        public static List<int> numbersAfterDot(string s)
        {
            List<int> output = new List<int>();
            string[] splitted = s.Split(',').Length > 1 ? s.Split(',') : s.Split('.');
            if (splitted.Length == 2)
            {
                foreach (char ch in splitted[1])
                {
                    output.Add(int.Parse(ch.ToString()));
                }
            }

            return output;
        }

        public static int Sum(List<int> nums)
        {

            const int numberCount = 3;
            int iterNum = 1;
            int result = 0;
            foreach (int item in nums)
            {
                if (iterNum <= numberCount)
                {
                    result += item;
                    iterNum++;
                }
                else
                {
                    break;
                }
            }
            return result;
        }
    }
}
