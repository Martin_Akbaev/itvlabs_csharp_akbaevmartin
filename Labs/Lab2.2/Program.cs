﻿using System;

// Составьте линейную программу, печатающую значение 1, если указанное высказывание является истинным, и 0 – в противном случае.
// Величина d является корнем только одного из уравнений ax2 + bx + c = 0 и mx + n = 0 относительно х.

namespace Lab2_2
{
    public class Equation
    {
        static void Main(string[] args)
        {
            do
            {
                try
                {
                    Console.Write("a: ");
                    double a = double.Parse(Console.ReadLine());

                    Console.Write("b: ");
                    double b = double.Parse(Console.ReadLine());

                    Console.Write("c: ");
                    double c = double.Parse(Console.ReadLine());

                    Console.Write("x: ");
                    double x = double.Parse(Console.ReadLine());

                    Console.Write("m: ");
                    double m = double.Parse(Console.ReadLine());

                    Console.Write("n: ");
                    double n = double.Parse(Console.ReadLine());

                    Console.Write("d: ");
                    double d = double.Parse(Console.ReadLine());

                    Console.WriteLine($"Result: { (Check(a, b, c, m, n, d)? 1 : 0) }");
                    Console.WriteLine("Press any key for continue, press Escape for exit.");

                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
            
        }

        public static bool Check(double a, double b, double c, double m, double n, double d)
        {
            const double EPSILON = 1e-10;

            double D = Math.Sqrt(b * b - 4 * a * c);
            double x1 = (-b + D) / (2 * a);
            double x2 = (-b - D) / (2 * a);

            return (Math.Abs((-n / m) - d) < EPSILON) ^ ((Math.Abs(x1 - d) < EPSILON) || (Math.Abs(x2 - d) < EPSILON));
        }
    }
}
