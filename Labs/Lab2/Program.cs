﻿using System;
using Lab2_3;
// Составьте программу вычисления стоимости поездки на автомобиле на дачу(туда и обратно). 
// Исходными данными являются: расстояние до дачи(в километрах); количество бензина, 
// которое потребляет автомобиль на 100 км пробега; цена одного литра бензина.
// Ниже представлен рекомендуемый вид диалога во время работы программы:
// Вычисление стоимости поездки на дачу.
// Расстояние до дачи (км) – 67
// Расход бензина (л на 100 км) – 8.5
// Цена литра бензина(руб.) – 23.7
// Поездка на дачу обойдется в 269 руб. 94 коп.

namespace Lab2_1
{
    public class TripCostCalc
    {
        private const int TOW_WAYS = 2;
        static void Main(string[] args)
        {
            do
            {
                Console.Write("Distance: ");
                if (double.TryParse(Console.ReadLine(), out double distance))
                {
                    Console.Write("Fuel consumption: ");
                    if (double.TryParse(Console.ReadLine(), out double fuelConsumption))
                    {
                        Console.Write("Price of gas: ");
                        if (double.TryParse(Console.ReadLine(), out double gasPrice))
                        {
                            double price = TripCost(distance: distance, fuelConsumption: fuelConsumption, petrolPrice: gasPrice) * TOW_WAYS;
                            Money money = new Money(price);
                            Console.WriteLine($"Trip cost: {money}");
                            Console.WriteLine("Press any key for continue, press Escape for exit.");
                            continue;
                        }
                    }
                }
                Console.WriteLine("Incorrect input!");
                Console.WriteLine("Press any key for continue, press Escape for exit.");

            } while (Console.ReadKey().Key != ConsoleKey.Escape);

        }

        public static double TripCost(double distance, double fuelConsumption, double petrolPrice)
        {
            const int HUNDRED_KM = 100;
            return distance * petrolPrice * fuelConsumption / HUNDRED_KM;
        }
    }
}
