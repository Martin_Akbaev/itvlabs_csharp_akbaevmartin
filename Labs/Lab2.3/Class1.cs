﻿using System;

namespace Lab2_3
{
    public class Money
    {
        public int Rubles { get; private set; }
        public int Kopecks { get; private set; }

        public Money(double money)
        {
            const int digits = 2;
            double roundedValue = Math.Round(money, digits);
            Rubles = (int)Math.Truncate(money);

            const int ONE_HUNDRED = 100;
            double tmp = ((roundedValue - (int)roundedValue) * ONE_HUNDRED);
            Kopecks = (int)Math.Round(tmp);
        }

        public override string ToString()
        {
            return $"{Rubles} rub. {Kopecks} kop.";
        }
    }
}
