using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lab1Test
{
    using Lab3;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestClass]
    public class UnitTestLab3
    {
        const int ARRAY_COUNT = 10;

        private void Check(List<double> sorted, bool reversed = false)
        {
            List<double> tmp = new List<double>(sorted);
            tmp.Sort();
            if (reversed)
            {
                tmp.Reverse();
            }
            Assert.IsTrue(tmp.SequenceEqual(sorted), "Error while checking order!");
        }

        [TestMethod]
        public void TestBubleSorterRandomList()
        {
            BubbleSorter sorter = new BubbleSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.Random)));
        }

        [TestMethod]
        public void TestMergeSorterRandomList()
        {
            MergeSorter sorter = new MergeSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.Random)));
        }

        [TestMethod]
        public void TestBubleSorterSortedList()
        {
            BubbleSorter sorter = new BubbleSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowUp)));
        }

        [TestMethod]
        public void TestMergeSorterSortedList()
        {
            MergeSorter sorter = new MergeSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowUp)));
        }

        [TestMethod]
        public void TestBubleSorterReversedList()
        {
            BubbleSorter sorter = new BubbleSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowDoun)));
        }

        [TestMethod]
        public void TestMergeSorterReversedList()
        {
            MergeSorter sorter = new MergeSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowDoun)));
        }

        [TestMethod]
        public void TestBubleSorterReverseList()
        {
            BubbleSorter sorter = new BubbleSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowUp), reverse: true), reversed: true);
        }

        [TestMethod]
        public void TestMergeSorterReverseList()
        {
            MergeSorter sorter = new MergeSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowUp), reverse: true), reversed: true);
        }

        [TestMethod]
        public void TestBubleSorterSortWithKey()
        {
            Func<double, double> customKey = val => -val;
            BubbleSorter sorter = new BubbleSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowUp), key: customKey), reversed: true);
        }

        [TestMethod]
        public void TestMergeSorterSortWithKey()
        {
            Func<double, double> customKey = val => -val;
            MergeSorter sorter = new MergeSorter();
            Check(sorter.Sort(DataGenerator.GenerateData(ARRAY_COUNT, order: DataGenerator.Order.ByGrowUp), key: customKey), reversed: true);
        }
    }

}