using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lab1Test
{
    using Lab2_3;
    [TestClass]
    public class UnitTestLab2_3
    {
        [TestMethod]
        public void TestCheckResultsForMoney()
        {
            const double INPUT = 112.567;
            const int etalonRub = 112;
            const int etalonKop = 57;

            Money money = new Money(INPUT);

            Assert.AreEqual(money.Rubles, etalonRub, $"Incorrect result for 'Rubles': {money.Rubles} != {etalonRub}");
            Assert.AreEqual(money.Kopecks, etalonKop, $"Incorrect result for 'Kopecks': {money.Kopecks} != {etalonKop}");
        }
    }

}