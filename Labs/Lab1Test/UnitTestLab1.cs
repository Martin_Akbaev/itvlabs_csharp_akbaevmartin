using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lab1Test
{
    using Lab1;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestClass]
    public class UnitTestLab1
    {
        [TestMethod]
        public void TestGetThreeNumbersAfterDot()
        {
            
            const string INPUT = "0.12345";
            List<int> result = SumNumbersAfterDot.numbersAfterDot(INPUT);
            List<int> Etalon = new List<int>() { 1, 2, 3, 4, 5 };

            string etalonStr = String.Join(",", Etalon);
            string resultStr = String.Join(",", result);
            Assert.IsTrue(Etalon.SequenceEqual(result), $"[{etalonStr}] != [{resultStr}]");
        }

        [TestMethod]
        public void TestSumOfThreeNumbersAfterDot()
        {

            List<int> INPUT = new List<int>() { 9, 9, 9, 9, 9 };
            const int ETALON = 27;
            int result = SumNumbersAfterDot.Sum(INPUT);

            Assert.AreEqual(ETALON, result, $"[{ETALON}] != [{result}]");
        }
    }

}